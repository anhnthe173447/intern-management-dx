
CREATE TABLE Account (
    [User_ID] int PRIMARY KEY,   -- Khóa chính (Primary Key)
    Role_ID INT,               -- ID vai trò (Role ID)
    Password VARCHAR(255),     -- Mật khẩu (Password)
    UserName VARCHAR(255)      -- Tên đăng nhập (UserName)
);


CREATE TABLE Roles (
    Role_ID INT PRIMARY KEY,   -- Khóa chính (Primary Key)
    RoleName NVARCHAR(255)      -- Tên vai trò (Role Name)
);


ALTER TABLE Account
ADD CONSTRAINT FK_Role
FOREIGN KEY (Role_ID) REFERENCES Roles(Role_ID);

Create table [Group](
	Group_ID int primary key,
	Group_Name nvarchar(100),
	Department_ID INT,
);

Create table Staff(
	Staff_ID int PRIMARY KEY,
	Manager_ID int,
	phone_number int,
	Account_ID int,
	Full_name nvarchar(100),
	Email nvarchar(255),
	Department_ID INT,


CREATE TABLE Account (
    [User_ID] int PRIMARY KEY,   -- Khóa chính (Primary Key)
    Role_ID INT,               -- ID vai trò (Role ID)
    Password VARCHAR(255),     -- Mật khẩu (Password)
    UserName VARCHAR(255),      -- Tên đăng nhập (UserName)
	FOREIGN KEY (Role_ID) REFERENCES Roles(Role_ID)
);

Create table Department(
	Department_ID INT PRIMARY KEY,
	Department_Name nvarchar(50)
);


Create table [Group](
	Group_ID int primary key,
	Group_Name nvarchar(100),
	Department_ID INT,
	FOREIGN KEY (Department_ID) REFERENCES Department(Department_ID),
);

create table Student(
Roll_number nvarchar(100) PRIMARY KEY,
Email nvarchar(225),
FullName nvarchar(225),
Phone_number nvarchar(15),
Major nvarchar(225),
Company nvarchar(225),
Job_tile nvarchar(225),
Link_CV nvarchar(225),
CV_Status Nvarchar(225),


);
create table Manager(
Manager_ID nvarchar(100) primary key,
phone_number nvarchar(15),
Full_name nvarchar(100),
Email nvarchar(225),
Account_ID INT,
id_Student nvarchar(100),
FOREIGN KEY (Account_ID) REFERENCES Account([User_ID]),
FOREIGN KEY (id_Student) REFERENCES Student(Roll_number),

);

Create table Staff(
	Staff_ID nvarchar(100) PRIMARY KEY,
	Manager_ID nvarchar(100),
	phone_number int,
	Account_ID int,
	Full_name nvarchar(100),
	Email nvarchar(255),
	Department_ID INT,
	Group_ID INT,
	FOREIGN KEY (Account_ID) REFERENCES Account([User_ID]),
	FOREIGN KEY (Manager_ID) REFERENCES Manager(Manager_ID),

);




Create table Intern(
	Intern_ID nvarchar(100) PRIMARY KEY,
	Account_ID INT,
	Date_start_work date,
	internship_End_Date date,
	Status_intern nvarchar(50),
	ID_student nvarchar(100),

	CV_intern nvarchar(1000),
	Division nvarchar(100),
	work_Allowance nvarchar(100),
	Department_ID nvarchar(50),
	Staff_Id int,
	Group_ID int

	CV_intern nvarchar(MAX),
	Division nvarchar(100),
	work_Allowance nvarchar(100),
	Department_ID nvarchar(50),
	Staff_Id nvarchar(100),
	Group_ID int,
	FOREIGN KEY (Account_ID) REFERENCES Account([User_ID]),
	FOREIGN KEY (Staff_Id) REFERENCES Staff(Staff_ID),
	FOREIGN KEY (Group_ID) REFERENCES [Group](Group_ID),


);

ALTER TABLE [Group]
ADD CONSTRAINT FK_Department1
FOREIGN KEY (Department_ID) REFERENCES Department(Department_ID);


create table Manager(
Manager_ID int primary key,

)


create table Survey_about_students(
evaluation_notes nvarchar(MAX),
Evaluate nvarchar(100),
id_intern nvarchar(100),
id_manager nvarchar(100),
id_Student nvarchar(100),
FOREIGN KEY (id_intern) REFERENCES Intern(Intern_ID),
FOREIGN KEY (id_manager) REFERENCES Manager(Manager_ID),
FOREIGN KEY (id_Student) REFERENCES Student(Roll_number),

);

create table Evaluation_fist_half_term(
id_Evaluation_fist INT Primary key,
Comment nvarchar(MAX),
Knowledge_and_skill INT,
Soft_skill INT,
Attitude INT,
Final_Result_2 FLOAT,
internship_status INTEGER CHECK(internship_status IN (0, 1)),

);

create table Evaluation_last_half_term(
id_Evaluation_last INT Primary key,
Comment nvarchar(MAX),
Knowledge_and_skill INT,
Soft_skill INT,
Attitude INT,
Final_Result_2 FLOAT,

);

create table Internship_Evaluations(
evaluation_notes nvarchar(MAX),
id_Evaluation_fist INT,
id_Evaluation_last INT,
Final_Result FLOAT,
id_intern nvarchar(100),
id_manager nvarchar(100),
id_Student nvarchar(100),
FOREIGN KEY (id_intern) REFERENCES Intern(Intern_ID),
FOREIGN KEY (id_manager) REFERENCES Manager(Manager_ID),
FOREIGN KEY (id_Student) REFERENCES Student(Roll_number),
FOREIGN KEY (id_Evaluation_fist) REFERENCES Evaluation_fist_half_term(id_Evaluation_fist),
FOREIGN KEY (id_Evaluation_last) REFERENCES Evaluation_last_half_term(id_Evaluation_last),

);

create table Project(
Project_ID INT Primary key,
project_name nvarchar(225),
Start_project Date,
End_Project DATE,
);

create table Internship_Task(
task_id INT Primary key,
id_intern nvarchar(100),
Project_id INT,
task_deadline DATE,
task_status INTEGER CHECK(task_status IN (0, 1)),
task_note nvarchar(MAX),
staff_id nvarchar(100),
FOREIGN KEY (staff_id) REFERENCES Staff(Staff_ID),
FOREIGN KEY (Project_id) REFERENCES Project(Project_ID)
);

